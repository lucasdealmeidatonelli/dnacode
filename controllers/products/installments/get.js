const knex = require("../../../db/knex");
const getPayment = require("../../../helpers/getPayment");

module.exports = async (request, response) => {
  const result = await knex
    .select("price")
    .from("products")
    .where({ code: request.query.code })
    .first();

  const value = result.price - request.query.entry;
  const interest = request.query.interest / 100;
  const installments = Number(request.query.installments);
  const payment = getPayment(value, interest, installments);

  const payments = Array(installments)
    .fill(undefined)
    .map((_, index) => ({
      installment: index + 1,
      payment: payment,
      interest: interest,
    }));

  response.json(payments);
};
