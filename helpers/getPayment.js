module.exports = (value, interest, installments = 1, entry = 0) => {
  return (
    (value - entry) * (interest / (1 - 1 / (1 + interest) ** installments))
  );
};
