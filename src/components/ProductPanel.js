import React from "react";

import axios from "axios";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";

import PaymentList from "./PaymentsList";
import PurchaseForm from "./PurchaseForm";

const ProductPanel = () => {
  const [state, setState] = React.useState({ payments: [] });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const form = new FormData(event.target);
    const params = [...form].reduce(
      (previousValue, currentValue) => ({
        ...previousValue,
        [currentValue[0]]: currentValue[1],
      }),
      {}
    );

    const payments = await axios.get("/products/installments", {
      params: { ...params },
    });

    setState({ payments: payments.data });
  };
  return (
    <Paper>
      <Box padding={2}>
        <PurchaseForm handleSubmit={handleSubmit} />
      </Box>

      {state.payments.length ? (
        <Box padding={2}>
          <PaymentList payments={state.payments} />
        </Box>
      ) : undefined}
    </Paper>
  );
};

export default ProductPanel;
