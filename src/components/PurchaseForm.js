import React from "react";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import useStyles from "../styles/root";

const PurchaseForm = (props) => {
  const classes = useStyles();

  return (
    <form className={classes.formControl} onSubmit={props.handleSubmit}>
      <TextField
        id="code"
        label="Código do produto"
        type="text"
        name="code"
        variant="outlined"
        fullWidth
        required
        inputProps={{ maxLength: 15 }}
      />
      <TextField
        id="entry"
        label="Entrada"
        type="number"
        name="entry"
        variant="outlined"
        fullWidth
        required
        inputProps={{ min: 0, step: 0.01 }}
      />
      <TextField
        id="installments"
        label="Parcelas"
        type="number"
        name="installments"
        variant="outlined"
        fullWidth
        required
        inputProps={{ min: 1, max: 24 }}
      />
      <TextField
        id="interest"
        label="Juros ao mês (%)"
        type="number"
        name="interest"
        variant="outlined"
        fullWidth
        required
        inputProps={{ min: 0, step: 0.01 }}
      />
      <Button type="submit" variant="contained">
        Calcular
      </Button>
    </form>
  );
};

export default PurchaseForm;
