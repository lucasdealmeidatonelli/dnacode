import React from "react";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import BRLFormat from "../helpers/BRLFormat";

const PaymentsList = (props) => {
  return (
    <TableContainer>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Parcela</TableCell>
            <TableCell align="center">Valor</TableCell>
            <TableCell align="center"> Juros (%)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.payments.map((row) => (
            <TableRow key={row.installment}>
              <TableCell component="th" scope="row">
                {row.installment}
              </TableCell>
              <TableCell align="center">
                {BRLFormat.format(row.payment)}
              </TableCell>
              <TableCell align="center">
                {String(row.interest * 100).replace(".", ",")}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default PaymentsList;
