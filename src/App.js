import React from "react";

import Container from "@material-ui/core/Container";

import ProductPanel from "./components/ProductPanel";

const App = () => {
  return (
    <Container maxWidth="sm">
      <ProductPanel />
    </Container>
  );
};

export default App;
