import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  formControl: {
    "& > * ": {
      margin: theme.spacing(1, 0),
    },
  },
}));

export default useStyles;
