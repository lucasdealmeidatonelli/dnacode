const format = new Intl.NumberFormat("pt-br", {
  style: "currency",
  currency: "BRL",
});

export default format;
