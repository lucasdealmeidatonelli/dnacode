exports.up = (knex) => {
  return knex.schema.createTable("products", (table) => {
    table.increments();
    table.string("code", 15);
    table.string("name", 150);
    table.decimal("price");
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable("products");
};
