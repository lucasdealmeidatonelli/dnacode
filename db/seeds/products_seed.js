exports.seed = (knex) => {
  // Deletes ALL existing entries
  return knex("products")
    .del()
    .then(() => {
      // Inserts seed entries
      return knex("products").insert([
        { code: "1A", name: "Product A", price: 1000.55 },
        { code: "2B", name: "Product B", price: 507.97 },
        { code: "3C", name: "Product C", price: 1220.38 },
      ]);
    });
};
