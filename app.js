const express = require("express");
const productsInstallmentsRouter = require("./router/products/installments/router");

const app = express();
const port = process.env.PORT || 5000;

app.use("/products/installments", productsInstallmentsRouter);

app.listen(port, () => {
  console.log(`Listening at port:${port}`);
});
