const express = require("express");

const router = express.Router();

const productsInstallmentsGet = require("../../../controllers/products/installments/get");

router.get("/", productsInstallmentsGet);

module.exports = router;
