# Setup/installation

Run `npm install`. After everything is installed, run `npm run setup-dev`.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `npm run dev`

Starts Node.js back-end concurrently with React front-end.

### `npm seed`

Runs all Knex seeds created and populates the database with mock data.

### `npm migrate`

Runs all Knex migrations created.

### `npm run setup-dev`

Executes a serie of scripts that configures the environment to develop and test locally. It executes, in sequence, `npm run migrate`, `npm run seed`, `npm run dev`.

# Routes

## /products/installments

### GET request example

```javascript
import axios from "axios";

axios.get("/products/installments", {
  params: {
    code: "1A",
    entry: 0,
    installments: 7,
    interest: 1.29,
  },
});
```

### Response example

```JSON
[
	{
		"installment": 1,
		"payment": 150.40571948168673,
		"interest": 0.0129
	},
	{
		"installment": 2,
		"payment": 150.40571948168673,
		"interest": 0.0129
	},
	{
		"installment": 3,
		"payment": 150.40571948168673,
		"interest": 0.0129
	},
	{
		"installment": 4,
		"payment": 150.40571948168673,
		"interest": 0.0129
	},
	{
		"installment": 5,
		"payment": 150.40571948168673,
		"interest": 0.0129
	},
	{
		"installment": 6,
		"payment": 150.40571948168673,
		"interest": 0.0129
	},
	{
		"installment": 7,
		"payment": 150.40571948168673,
		"interest": 0.0129
	}
]

```
